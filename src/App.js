import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';

import logo from './logo.svg';
import './App.css';

class App extends Component {
  render() {
    return (
        <div className="App">
            <Router>
                <Route path="/about" component={AboutMenu}/>
            </Router>
            <Router>
                <div>
                    <Route path="/" exact component={Index}/>
                    <Route path="/about" component={About}/>
                </div>
            </Router>
        </div>
    );
  }
}

const Index = ({ match }) => (
    <div className="Home">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <p className="App-intro">
          To get started, edit <code>src/App.js</code> and save to reload.
        </p>
        <a href="/about">About</a>
    </div>
);

const About = ({ match }) => (
    <div className="About">
        <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h1 className="App-title">About React</h1>
        </header>
        <p className="App-intro">
            To get started, edit <code>src/App.js</code> and save to reload.
        </p>
    </div>
);

const AboutMenu = ({ match }) => (
    <div className="AboutMenu">
        HEADER
    </div>
);

export default App;
